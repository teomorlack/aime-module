import { libWrapper } from './lib/libWrapper/shim.js';

// Handlebars helpers

// less than
Handlebars.registerHelper('lst', function( a, b ){
	var next =  arguments[arguments.length-1];
	return (a < b) ? next.fn(this) : next.inverse(this);
});
// greater than
Handlebars.registerHelper('grt', function( a, b ){
	var next =  arguments[arguments.length-1];
	return (a > b) ? next.fn(this) : next.inverse(this);
});
// equal than
Handlebars.registerHelper('eqt', function( a, b ){
	var next =  arguments[arguments.length-1];
	return (a == b) ? next.fn(this) : next.inverse(this);
});

Hooks.on('init', async function () {
    
	//DO CONFIG CHANGES
	CONFIG.DND5E.limitedUsePeriods = {
		"sr": "DND5E.ShortRest",
		"lr": "DND5E.LongRest",
		"day": "DND5E.Day",
		"charges": "DND5E.Charges",
		"jrny": "AIME.Journey",
		"adv": "AIME.Adventure",
	};
	CONFIG.DND5E.consumableTypes = {
		"ammo": "DND5E.ConsumableAmmunition",
		"potion": "DND5E.ConsumablePotion",
		"poison": "DND5E.ConsumablePoison",
		"food": "DND5E.ConsumableFood",
		//"scroll": "DND5E.ConsumableScroll",
		//"wand": "DND5E.ConsumableWand",
		//"rod": "DND5E.ConsumableRod",
		"trinket": "DND5E.ConsumableTrinket"
	};
	
	CONFIG.DND5E.currencies = {
	  gp: {
	    label: "AIME.CoinsGP",
	    abbreviation: "AIME.CoinsAbbrGP"
	  },
	  sp: {
	    label: "AIME.CoinsSP",
	    abbreviation: "AIME.CoinsAbbrSP",
	    conversion: {into: "gp", each: 20}
	  },
	  cp: {
	    label: "AIME.CoinsCC",
	    abbreviation: "AIME.CoinsAbbrCC",
	    conversion: {into: "sp", each: 12}
	  }
	};

	CONFIG.DND5E.skills = {
		"acr": "DND5E.SkillAcr",
		"ani": "DND5E.SkillAni",
		"ath": "DND5E.SkillAth",
		//"arc": "DND5E.SkillArc",
		"dec": "DND5E.SkillDec",
		"his": "DND5E.SkillHis",
		"ins": "AIME.SkillIns",
		"itm": "DND5E.SkillItm",
		"inv": "DND5E.SkillInv",
		"lor": "AIME.SkillLor",
		"med": "DND5E.SkillMed",
		"nat": "DND5E.SkillNat",
		"prc": "DND5E.SkillPrc",
		"prf": "DND5E.SkillPrf",
		"per": "DND5E.SkillPer",
		"rid": "AIME.SkillRid",
		//"rel": "DND5E.SkillRel",
		"sha": "AIME.SkillSha",
		"slt": "DND5E.SkillSlt",
		"ste": "DND5E.SkillSte",
		"sur": "DND5E.SkillSur",
		"tra": "AIME.SkillTra"
	};


	CONFIG.DND5E.abilities = {
		"str": "DND5E.AbilityStr",		
		"dex": "DND5E.AbilityDex",		
		"con": "DND5E.AbilityCon",
		"int": "DND5E.AbilityInt",
		"wis": "DND5E.AbilityWis",
		"cha": "DND5E.AbilityCha",
		"sha": "AIME.AbilitySha",
		"perm": "AIME.AbilityPerm"
	};

	CONFIG.DND5E.abilityAbbreviations = {
	  "str": "DND5E.AbilityStrAbbr",
	  "dex": "DND5E.AbilityDexAbbr",
	  "con": "DND5E.AbilityConAbbr",
	  "int": "DND5E.AbilityIntAbbr",
	  "wis": "DND5E.AbilityWisAbbr",
	  "cha": "DND5E.AbilityChaAbbr",
	  "sha": "DND5E.AbilityShaAbbr",
	  "perm": "DND5E.AbilityPermAbbr"
	};


	CONFIG.DND5E.languages = {
		"common": "AIME.LanguagesCommon",
		"blackspeech": "AIME.LanguagesBlackSpeech",
		"ancient": "AIME.LanguagesQuenya",
		"sindarin": "AIME.LanguagesSindarin",
		"dalish": "AIME.LanguagesDalish",
		"vale": "AIME.LanguagesVale",
		"dwarvish": "AIME.LanguagesDwarvish",
		"woodland": "AIME.LanguagesWoodland",
		"rohan": "AIME.LanguagesRohan",
		"orkish": "AIME.LanguagesOrkish"
	};


	CONFIG.DND5E.newSkills = [
		{
			"skl": "lor",
			"ability": "int"
		},
		{
			"skl": "rid",
			"ability": "int"
		},
		{
			"skl": "sha",
			"ability": "int"
		},
		{
			"skl": "tra",
			"ability": "int"
		},
	];
	CONFIG.DND5E.newScores = [
		{
			"scr": "sha",
			"proficient": "0"
		},
		{
			"scr": "perm",
			"proficient": "0"
		},
	];

	const rpgUI = game.modules?.get('rpg-styled-ui')?.active;
	const tidy5eModule = game.modules?.get('tidy5e-sheet')?.active;

	if (tidy5eModule === true) {
    		loadTemplates([
    		'modules/aime/templates/aime-tidy5e-standard.hbs',
    		 ]);
    	}
    if (rpgUI === true) {
    	loadTemplates([
    		'modules/aime/templates/aime-miserable-box-rpgui.hbs',
    		]);
    	}
    loadTemplates([
    	'modules/aime/templates/aime-miserable-box.hbs',
    	'modules/aime/templates/aime-scores-end.hbs',
    	'modules/aime/templates/aime-living-standard.hbs'
  	]);

	CONFIG.DND5E.delSkills = ["arc", "rel", "tss", "tst"];

	// Remove PP and EP from showing up on character sheet displays since we don't use them in AiME	
	const originalCGetData = game.dnd5e.applications.ActorSheet5eCharacter.prototype.getData;
	libWrapper.register("aime", "game.dnd5e.applications.ActorSheet5eCharacter.prototype.getData", function patchedActorSheet5eCharacter(wrapped, ...args) {

		const data = originalCGetData.call(this);
		delete data.data.currency.pp;
		delete data.data.currency.ep;


		// Return data to the sheet
		return data
	}, "MIXED");

	// Remove PP and EP from showing up on vehicle sheet displays since we don't use them in AiME	
	const originalVGetData = game.dnd5e.applications.ActorSheet5eVehicle.prototype.getData;
	libWrapper.register("aime", "game.dnd5e.applications.ActorSheet5eVehicle.prototype.getData", function patchedActorSheet5eCharacter(wrapped, ...args) {

		const data = originalVGetData.call(this);
		delete data.data.currency.pp;
		delete data.data.currency.ep;


		// Return data to the sheet
		return data
	}, "MIXED");

   libWrapper.register("aime", "CONFIG.Actor.documentClass.prototype._preCreate", function patchedPreCreate(wrapped, ...args) {
        wrapped(...args);

        this.data.update({
            data: {
                abilities: {
                    sha: {
                    	"value": 0,
                        "proficient": 0
                  	},
                    perm: {
                        "value": 0,
                        "proficient": 0
                  	}                 	
                }
            }
        });       
    }, "WRAPPER");

	libWrapper.register("aime", "CONFIG.Actor.documentClass.prototype.prepareDerivedData", function patchedPrepareDerivedData(wrapped, ...args) {
	const actorData = this.data;
	const data = actorData.data;
	const source = actorData._source;
	const flags = actorData.flags.dnd5e || {};
	const bonuses = getProperty(data, "bonuses.abilities") || {};
	const delSkills = ["arc", "rel", "tss", "tst"];

	// Retrieve data for polymorphed actors
    let originalSaves = null;
    let originalSkills = null;
    if (this.isPolymorphed) {
      const transformOptions = this.getFlag('dnd5e', 'transformOptions');
      const original = game.actors?.get(this.getFlag('dnd5e', 'originalActor'));
      if (original) {
        if (transformOptions.mergeSaves) {
          originalSaves = original.data.data.abilities;
        }
        if (transformOptions.mergeSkills) {
          originalSkills = original.data.data.skills;
        }
      }
    }

	// Ability modifiers and saves
    const dcBonus = Number.isNumeric(data.bonuses?.spell?.dc) ? parseInt(data.bonuses.spell.dc) : 0;
    const saveBonus = Number.isNumeric(bonuses.save) ? parseInt(bonuses.save) : 0;
    const checkBonus = Number.isNumeric(bonuses.check) ? parseInt(bonuses.check) : 0;
    for (let [id, abl] of Object.entries(data.abilities)) {
      if ( flags.diamondSoul ) abl.proficient = 1;  // Diamond Soul is proficient in all saves
      abl.mod = Math.floor((abl.value - 10) / 2);
      abl.prof = (abl.proficient || 0) * data.attributes.prof;
      abl.saveBonus = saveBonus;
      abl.checkBonus = checkBonus;
      abl.save = abl.mod + abl.prof + abl.saveBonus;
      abl.dc = 8 + abl.mod + data.attributes.prof + dcBonus;

      // If we merged saves when transforming, take the highest bonus here.
      if (originalSaves && abl.proficient) {
        abl.save = Math.max(abl.save, originalSaves[id].save);
      }
    }
		//FIX CUSTOM SKILL ABILITIES
		const newSkills = CONFIG.DND5E.newSkills;
		const newScores = CONFIG.DND5E.newScores;
		
		if ( this.type != "vehicle" ) {
		newSkills.forEach(e => {
			let sklName = e["skl"];
			let sklAbility = e["ability"];
			if (typeof (data.skills[sklName]) == "undefined") {
				data.skills[sklName] = new Object();
				data.skills[sklName].value = 0;
				data.skills[sklName].ability = sklAbility;
			}
			if (typeof (data.skills[sklName].ability) == "undefined") {
				data.skills[sklName].ability = [sklAbility];
			}
		});
		newSkills.forEach(e => {
		let sklName = e["skl"];
		let sklAbility = e["ability"];
		if (typeof (source.data.skills[sklName]) == "undefined") {
			source.data.skills[sklName] = new Object();
			source.data.skills[sklName].value = 0;
			source.data.skills[sklName].ability = sklAbility;
		}
		if (typeof (source.data.skills[sklName].ability) == "undefined") {
			source.data.skills[sklName].ability = [sklAbility];
	}
	});
		newScores.forEach(e => {
		let scrName = e["scr"];
		if (typeof (source.data.abilities[scrName]) == "undefined") {
			source.data.abilities[scrName] = new Object();
			source.data.abilities[scrName].value = 0;
			source.data.abilities[scrName].proficient = 0;
		}
		if (typeof (source.data.abilities[scrName].proficient) == "undefined") {
			source.data.abilities[scrName].proficient = 0;
	}
	});
	
		delSkills.forEach(e => {
			if (typeof (data.skills[e]) != "undefined") {
				delete data.skills[e];
			}
		});

		// Fix specifically for a migration case, will not affect anyone else
		// Check if custom skills x1 and x2 exist, if yes, remove them
		if (data.skills["x1"] != null) {
			delete data.skills["x1"]
		}
		if (data.skills["x2"] != null) {
			delete data.skills["x2"]
		}

		// Check if Shadow ability (=character attribute) exists, otherwise add it
		if ( this.type === "character" ) {
			if (data.abilities["sha"] == null) {
				data.abilities["sha"] = new Object();
				data.abilities["sha"].value = 0;
				data.abilities["sha"].proficient = 0;
				data.abilities["sha"].prof = 0;
				data.abilities["sha"].saveBonus = 0;
				data.abilities["sha"].save = 0;
				data.abilities["sha"].mod = 0;
				data.abilities["sha"].checkBonus = 0;
			}
 		
			// Check if Shadow Permanent ability (=character attribute) exists, otherwise add it
			if (data.abilities["perm"] == null) {
				data.abilities["perm"] = new Object();
				data.abilities["perm"].value = 0;
				data.abilities["perm"].proficient = 0;
				data.abilities["perm"].prof = 0;
				data.abilities["perm"].saveBonus = 0;
				data.abilities["perm"].save = 0;
				data.abilities["perm"].mod = 0;
				data.abilities["perm"].checkBonus = 0;
			}
		}

		// Check if Shadow and Shadow Permanent ability exists on NPC, then delete it
		// if ( this.type === "npc" ) {
		// 	if (data.abilities["sha"] != null) {
		// 		delete data.abilities["sha"]
		// 	}
		// 	if (data.abilities["perm"] != null) {
		// 		delete data.abilities["perm"]
		// 	}		
		// }
	}
	

			// Check if Shadow and Shadow Permanent ability exists on Vehicle, then delete it
		// if ( this.type === "vehicle" ) {
		// 	if (data.abilities["sha"] != null) {
		// 		delete data.abilities["sha"]
		// 	}
		// 	if (data.abilities["perm"] != null) {
		// 		delete data.abilities["perm"]
		// 	}
		// }
		


		//console.log(data.skills);

		//END FIX BACK TO REGULAR	

		// Inventory encumbrance
   		data.attributes.encumbrance = this._computeEncumbrance(actorData);

   		// Prepare skills
		this._prepareSkills(actorData, bonuses, checkBonus, originalSkills);

		// Reset class store to ensure it is updated with any changes
    	this._classes = undefined;

	    // Determine Initiative Modifier
	    const init = data.attributes.init;
	    const athlete = flags.remarkableAthlete;
	    const joat = flags.jackOfAllTrades;
	    init.mod = data.abilities.dex.mod;
	    if ( joat ) init.prof = Math.floor(0.5 * data.attributes.prof);
	    else if ( athlete ) init.prof = Math.ceil(0.5 * data.attributes.prof);
	    else init.prof = 0;
	    init.value = init.value ?? 0;
	    init.bonus = init.value + (flags.initiativeAlert ? 5 : 0);
	    init.total = init.mod + init.prof + init.bonus;

		// Cache labels
	    this.labels = {};
	    if ( this.type === "npc" ) {
	      this.labels["creatureType"] = this.constructor.formatCreatureType(data.details.type);
	    }

	    // Prepare spell-casting data
    	this._computeSpellcastingProgression(this.data);

	    // Prepare armor class data
	    const {armor, shield} = this._computeArmorClass(data);
	    this.armor = armor || null;
	    this.shield = shield || null;

        return wrapped(...args);
    }, "WRAPPER");
});

function i18n(key) {
	return game.i18n.localize(key);
}

// function migrateActorData(actor) {
// 	let updateData = {};

// 	if (actor.type != "npc") {
// 		return updateData;
// 	}

// 	let actorData = actor.data;
// 	if ()
// }

Hooks.on('renderActorSheet', async function (app, html, data) {
	const sheet5e = app.options.classes.join();
	const sheetTidy = app.options.classes[0];
	const sheetTidyType = app.options.classes[3];
	const rpgUI = game.modules?.get('rpg-styled-ui')?.active;

	if (sheet5e === "dnd5e,sheet,actor,character") {
		if (rpgUI === false) {
		const misBox = "/modules/aime/templates/aime-miserable-box.hbs"
		const misHtml = await renderTemplate(misBox, data);
		var inspDiv = $(html).find(".flexrow.inspiration");
		inspDiv[0].outerHTML = misHtml;
		}

        const scoreBox = "/modules/aime/templates/aime-scores-end.hbs"
        const scoreHtml = await renderTemplate(scoreBox, data);
        var abiScores = $(html).find(".ability-scores.flexrow");
        var endScores = $(html).find( ".ability-scores.flexrow li" ).slice(6);
        endScores.remove()
		abiScores.append(scoreHtml)

        const livingBox = "/modules/aime/templates/aime-living-standard.hbs"
        const livingHtml = await renderTemplate(livingBox, data);
        var alignment = $(html).find('*[name="data.details.alignment"]').parent()[0];
        alignment.innerHTML = livingHtml;

        //Remove spellbook
        $(html).find('*[data-tab="spellbook"]').remove()

        $(html).find(".dnd5e.sheet.actor.character").css("min-height", "823px");

        //RPG-Styled UI Compatibility
        if (rpgUI === true) {
        	const ctrlDiv = '<div class="control-group"></div>';
        	const ctrlEndDiv = '</div>';
        	const ctrlLabel = '<label class="control control-checkbox"></label>';
        	const ctrlEndLabel = '</label>';
        	const ctrlIndicator = '<div class="control_indicator ctrlNew"></div>'
        	const resInput = $(html).find(".recharge.checkbox.flexcol input")
        	const misBox = "/modules/aime/templates/aime-miserable-box-rpgui.hbs"
			const misHtml = await renderTemplate(misBox, data);
			var inspDiv = $(html).find(".flexrow.inspiration");
			$(".dnd5e.sheet.actor.character").css("min-height", "823px");
			$(html).find(".encumbrance.encumbered").css("background", "#ffffff14");
			$(html).find(".encumbrance-bar").css({"background": "#331914","border": "1px solid #372d25"})
			$(html).find(".encumbrance-label").css("font-size", "10px")
			$(html).find(".ability3").css("border", "2px groove var(--border-color)");
			resInput.wrap( ctrlDiv ).wrap( ctrlLabel ).after( ctrlIndicator );
			inspDiv[0].outerHTML = misHtml;
        };
    }
    	if (sheet5e === "dnd5e,sheet,actor,npc") {
    		var npcSha = $(html).find('[data-ability="sha"]');
    		var npcPerm = $(html).find('[data-ability="perm"]');
        	$(html).find('*[name="data.details.alignment"]').parent().remove()
        	npcSha.remove();
        	npcPerm.remove();
        }

        if (sheet5e === "dnd5e,sheet,actor,vehicle") {
    		var npcSha = $(html).find('[data-ability="sha"]');
    		var npcPerm = $(html).find('[data-ability="perm"]');
        	npcSha.remove();
        	npcPerm.remove();
        }

        // MonsterBlock Compatibility
        if (sheetTidy === "monsterblock") {
	        	var npcSha = $(html).find('[data-ability="sha"]');
	    		var npcPerm = $(html).find('[data-ability="perm"]');
	        	npcSha.remove();
	        	npcPerm.remove();
	    }

        // Tidy5e Sheet Compatibility
        if (sheetTidy === "tidy5e") {
			const livingBox = "/modules/aime/templates/aime-tidy5e-standard.hbs"
			const livingHtml = await renderTemplate(livingBox, data);
			const tidyMisBox = "/modules/aime/templates/aime-tidy5e-miserable.hbs"
			const tidyMisHtml = await renderTemplate(tidyMisBox, data);
			const tidyGP = "/modules/aime/templates/aime-tidy5e-gp.hbs"
			const tidyGPRender =  await renderTemplate(tidyGP, data);
			const tidySP = "/modules/aime/templates/aime-tidy5e-sp.hbs"
			const tidySPRender =  await renderTemplate(tidySP, data);
			const tidyCP = "/modules/aime/templates/aime-tidy5e-cp.hbs"
			const tidyCPRender =  await renderTemplate(tidyCP, data);
			var tidyAlignment = $(html).find('[data-target*="alignment"]');
			var tidyAlignmentInput = $(html).find('input[name="data.details.alignment"]')[0];
			var tidyBackground = $(html).find('[data-target*="background"]');
			var tidyInspiration = $(html).find('.inspiration');

			// Remove alignment and insert standard of living
			if (sheetTidyType != "vehicle") {
			tidyAlignment.remove();
			tidyAlignmentInput.remove();
			}
			// If NPC or Vehicle remove Shadow and Perm scores
			if (sheetTidyType != "character") {
			var npcSha = $(html).find('[data-ability="sha"]');
    		var npcPerm = $(html).find('[data-ability="perm"]');
        	npcSha.remove();
        	npcPerm.remove();
			}
			if (sheetTidyType === "character") {
			tidyBackground.after(livingHtml);

			// Remove mod/save box from new scores
			var tidySha = $(html).find('[data-ability="sha"]').find('.value-footer');
			var tidyPerm = $(html).find('[data-ability="perm"]').find('.value-footer');
			var tidyCogPerm = $(html).find('[data-ability="perm"]').find('.config-button');
			var tidyCogSha = $(html).find('[data-ability="sha"]').find('.config-button');
			tidySha.remove();
			tidyPerm.remove();
			tidyCogSha.remove();
			tidyCogPerm.remove();

			// Add Miserable button next to Inspiration button
			tidyInspiration.after(tidyMisHtml);

			// Remove spellbook tab
			$(html).find('[data-tab="spellbook"]').remove()	

			// Change currency abbreviations
			var tidyGPReplace = $(html).find('.denomination.gp')[0];
			tidyGPReplace.innerHTML = tidyGPRender;
			var tidySPReplace = $(html).find('.denomination.sp')[0];
			tidySPReplace.innerHTML = tidySPRender;
			var tidyCPReplace = $(html).find('.denomination.cp')[0];
			tidyCPReplace.innerHTML = tidyCPRender;
			}
        }
    });
